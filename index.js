'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const sequelize = new Sequelize('sequelize_proiect','root','',{
	dialect : 'mysql',
	define : {
		timestamps : false
	}
})

const Serial = sequelize.define('serial', {
	name : {
		type : Sequelize.STRING(100),
		allowNull : false,
		validate : {
			len : [3, 100]
		}
	},
	overview: {
		type : Sequelize.TEXT
	},
	userScore : {
		type : Sequelize.FLOAT,
		allowNull : false,
		validate : {
			min:1,
			max:100
		}
	},
	 releaseDate : {
		type : Sequelize.DATE,
		allowNull : false,
	}
})

const Genre = sequelize.define('genre', {
	name : {
		type : Sequelize.STRING,
		allowNull : false
	}
})

const Review = sequelize.define('review', {
    user : {
	    type : Sequelize.STRING,
	    allowNull : false
	},
	serial : {
	    type : Sequelize.STRING,
	    allowNull : false
	},
	commentText : {
	    type: Sequelize.TEXT,
	    allowNull : false
	}
})

const User = sequelize.define('user', {
    name: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   surname: {
        type: Sequelize.STRING,
        allowNull: false
   },
	username : {
		type : Sequelize.STRING(100),
		allowNull : false,
		len: [3,50]
	},
	password: {
	    type: Sequelize.STRING,
	    allowNull:false,
	    len:[5,50]
	},
})

Serial.hasMany(Genre)
Serial.hasMany(Review)

const app = express()
app.use(bodyParser.json())

app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'created'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/serials', async (req, res) => {
	try{
		let params =  {
			where : {},
			order : [
				['name','ASC'],
				['genre','ASC']
			]
		}
		let pageSize = 10
		if (req.query){
			if (req.query.filter){
				params.where.name = {
		            [Op.like] : `%${req.query.filter}%`
		        }
			}
			if (req.query.pageSize){
				pageSize = parseInt(req.query.pageSize)
			}
			if (req.query.pageNo){
				params.limit = pageSize
				params.offset = pageSize * parseInt(req.query.pageNo)
			}
		}
		let serials = await Serial.findAll(params)
		res.status(200).json(serials)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/serials', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await Serial.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await Serial.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/serials/:id', async (req, res) => {
	try{
		let serial = await Serial.findById(req.params.id)
		if (serial){
			res.status(200).json(serial)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/serials/:id', async (req, res) => {
	try{
		let serial = await Serial.findById(req.params.id)
		if (serial){
			await serial.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/serials/:id', async (req, res) => {
	try{
		let serial = await Serial.findById(req.params.id)
		if (serial){
			await serial.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/serials/:id/genres', async (req, res) => {
	try {
		let serial = await Serial.findById(req.params.id)
		if (serial){
			let genres = await	serial.getGenres()
			res.status(200).json(genres)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/serials/:id/genres', async (req, res) => {
	try {
		let serial = await Serial.findById(req.params.id)
		if (serial){
			let genre = req.body
			genre.studentId = serial.id
			await Genre.create(genre)
			res.status(201).json({message: 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/serials/:id/reviews', async (req, res) => {
	try {
		let serial = await Serial.findById(req.params.id)
		if (serial){
			let reviews = await	serial.getReviews()
			res.status(200).json(reviews)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/serials/:id/reviews', async (req, res) => {
	try {
		let serial = await Serial.findById(req.params.id)
		if (serial){
			let review = req.body
			review.serialId = serial.id
			await Review.create(review)
			res.status(201).json({message: 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/users', async (req, res) => {
	try{
		let params =  {
			where : {},
			order : [
				['username','ASC']
			]
		}
		let pageSize = 10
		if (req.query){
			if (req.query.filter){
				params.where.username = {
		            [Op.like] : `%${req.query.filter}%`
		        }
			}
			if (req.query.pageSize){
				pageSize = parseInt(req.query.pageSize)
			}
			if (req.query.pageNo){
				params.limit = pageSize
				params.offset = pageSize * parseInt(req.query.pageNo)
			}
		}
		let users = await User.findAll(params)
		res.status(200).json(users)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/users', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await User.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await User.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/users/:id', async (req, res) => {
	try{
		let user = await User.findById(req.params.id)
		if (user){
			res.status(200).json(user)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/usesrs/:id', async (req, res) => {
	try{
		let user = await User.findById(req.params.id)
		if (user){
			await user.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/users/:id', async (req, res) => {
	try{
		let user = await User.findById(req.params.id)
		if (user){
			await user.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.listen(8080)

